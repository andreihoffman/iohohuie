package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.model.TasksOperations;
import tasks.repository.TasksRepository;

import java.util.Date;
import java.util.List;

public class TasksService {

    private ArrayTaskList tasks;
    private TasksRepository repo;
    public TasksService(ArrayTaskList tasks){
        this.tasks = tasks;
    }


    public ObservableList<Task> getObservableList(){
        return FXCollections.observableArrayList(tasks.getAll());
    }
    public String getIntervalInHours(Task task){
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return formTimeUnit(hours) + ":" + formTimeUnit(minutes);//hh:MM
    }
    public String formTimeUnit(int timeUnit){
        StringBuilder sb = new StringBuilder();
        if (timeUnit < 10) sb.append("0");
        if (timeUnit == 0) sb.append("0");
        else {
            sb.append(timeUnit);
        }
        return sb.toString();
    }

    public int parseFromStringToSeconds(String stringTime){//hh:MM
        String[] units = stringTime.split(":");
        int hours = Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]);

        return (hours * DateService.MINUTES_IN_HOUR + minutes) * DateService.SECONDS_IN_MINUTE;

    }

    public Iterable<Task> filterTasks(Date start, Date end){
        TasksOperations tasksOps = new TasksOperations(getObservableList());
        //Iterable<Task> filtered = tasks.incoming(start, end);

        return tasksOps.incoming(start,end);
    }

    public void addTask(Task newTask)
    {
        tasks.add(newTask);
    }

    public boolean deleteTask(Task task)
    {
        try{
            tasks.remove(task);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public void updateTask(Task updatedTask) {repo.updateTask(updatedTask);}

    public Task getTask(int id)
    {
        return repo.getTask(id);
    }
    public List<Task> getAllTasks()
    {
        return tasks.getAll();
    }

}
