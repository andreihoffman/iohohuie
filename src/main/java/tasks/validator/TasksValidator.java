package tasks.validator;

import org.apache.log4j.Logger;
import tasks.model.Task;

public class TasksValidator {
    private static final Logger log = Logger.getLogger(Task.class.getName());
    public void validate(Task task) {
        String errors="";
        if (task.getTitle() == ""){
            log.error("Title cannot be empty");
            errors+="Title cannot be empty\n";
        }
        if(task.getTitle().length()>255){
            log.error("Title is too long");
            errors+="Title is too long\n";
        }
        if (task.getInterval() < 1) {
            log.error("Interval < than 1");
            errors+="Interval should be >= 1\n";
        }
//        if(task.getInterval()>366){
//            log.error("Interval > than 366");
//            errors+="Interval should be <= 366\n";
//        }
        if(!errors.equals("")) throw new IllegalArgumentException(errors);
    }

}
