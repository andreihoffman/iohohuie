package tasks.model;

public class Error extends Throwable{
    Error(String message)
    {
        super(message);
    }
}
