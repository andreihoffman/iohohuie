package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.repository.TasksRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;

public class TaskServiceMockTest {
    private ArrayTaskList repo;
    private TasksService service;
    private SimpleDateFormat sdf;
    private Date start, end;
    @BeforeEach
    void setUp() {
        sdf= Task.getDateFormat();
        try {
            start=sdf.parse("2022-03-29 12:00");
            end=sdf.parse("2022-08-29 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }

        repo = Mockito.mock(ArrayTaskList.class);
        service = new TasksService(repo);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addTaskServiceTestMock(){ //unit testing
//        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
//        Task t1=new Task("Task2", sdf.parse("2022-04-16 12:00"), sdf.parse("2022-07-20 10:00"), 13);
        Task t=mock(Task.class);
        Task t1=mock(Task.class);

        Mockito.doNothing().when(repo).add(t);
        assert service.getObservableList().size()==0;
        Mockito.when(repo.getAll()).thenReturn(Arrays.asList(t1));
        assert service.getObservableList().size()==1;

        service.addTask(t1);
        Mockito.verify(repo,times(1)).add(t1);
        Mockito.verify(repo,times(0)).add(t);
    }

    @Test
    void addTaskServiceTest2Mock(){ //unit testing
//        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
//        Task t1=new Task("Task2", sdf.parse("2022-04-16 12:00"), sdf.parse("2022-07-20 10:00"), 13);
        Task t=mock(Task.class);
        Mockito.doNothing().when(repo).add(t);
        Mockito.when(repo.getAll()).thenReturn(Arrays.asList());
        Mockito.when(repo.getAll()).thenReturn(Arrays.asList());
        assert service.getAllTasks().size()==0;
    }
    @Test
    void filterServiceTestMock() throws ParseException { //unit testing
        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
        ObservableList<Task> l= FXCollections.observableArrayList();
        l.add(t);
        Mockito.when(service.filterTasks(start,end)).thenReturn(l);
        ArrayList<Task> list = (ArrayList<Task>) service.filterTasks(start,end);

        assertEquals(0,list.size());
        assertEquals(1,service.getAllTasks().size());
    }
}
