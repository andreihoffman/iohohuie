package tasks.repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.services.TasksService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.mockito.Mockito.*;

//teste pentru ArrayTaskList
public class TaskRepositoryMockTest {
    private ArrayTaskList repo;
    private SimpleDateFormat sdf;
    private TasksService service;
    @BeforeEach
    void setUp() {
        repo = mock(ArrayTaskList.class);
        service = new TasksService(repo);
        sdf= Task.getDateFormat();
    }

    @Test
    void addTaskRepoTest1Mock(){ //unit testing
//        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
        Task t=mock(Task.class);

        ObservableList<Task> l= FXCollections.observableArrayList();
        Mockito.when(repo.getAll()).thenReturn(l);
        Mockito.doNothing().when(repo).add(t);

        Mockito.verify(repo,times(0)).add(t);
        Mockito.verify(repo,never()).getAll();
        assert (repo.getAll().size() == 0);
    }

    @Test
    void addTaskRepoTest2Mock(){ //unit testing
//        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
//        Task t1=new Task("Task2", sdf.parse("2022-04-16 12:00"), sdf.parse("2022-07-20 10:00"), 13);
        Task t=mock(Task.class);
        Task t1=mock(Task.class);

        Mockito.when(repo.getAll()).thenReturn(Arrays.asList(t));
        Mockito.doNothing().when(repo).add(t1);
        repo.add(t);
        Mockito.verify(repo,times(1)).add(t);
        Mockito.verify(repo,times(0)).add(t1);
        Mockito.verify(repo,never()).getAll();
        assert (repo.getAll().size() == 1);
    }

    @Test
    void removeTaskRepoTestMock(){ //unit testing
//        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
//        Task t1=new Task("Task2", sdf.parse("2022-04-16 12:00"), sdf.parse("2022-07-20 10:00"), 13);
        Task t=mock(Task.class);
        Task t1=mock(Task.class);

        Mockito.when(repo.getAll()).thenReturn(Arrays.asList());
        Mockito.when(repo.remove(t1)).thenReturn(false);
        Mockito.when(repo.remove(t)).thenReturn(true);

//        repo.remove(t1);
//        repo.remove(t);
//        Mockito.verify(repo,times(1)).remove(t1);
//        Mockito.verify(repo,times(1)).remove(t);
        Mockito.verify(repo,never()).getAll();
        assert(repo.remove(t1) == false);
        assert(repo.remove(t) == true);
        assert (repo.getAll().size() == 0);
    }

    @Test
    void step2IntegrationAddTest(){//integration testing
        //integrare R (se testează S cu R; pentru E se folosesc obiecte mock);
        ArrayTaskList arrayTaskList =new ArrayTaskList();
        TasksService tasksService = new TasksService(arrayTaskList);
        Task t = mock(Task.class);
        tasksService.addTask(t);
        assert(tasksService.getAllTasks().size()==1);
    }

    @Test
    void step2IntegrationRemoveTest(){//integration testing
        //integrare R (se testează S cu R; pentru E se folosesc obiecte mock);
        ArrayTaskList arrayTaskList =new ArrayTaskList();
        TasksService tasksService = new TasksService(arrayTaskList);
        Task t = mock(Task.class);
        tasksService.addTask(t);
        assert(tasksService.deleteTask(t) == true);
        assert(tasksService.getAllTasks().size()==0);
    }
}