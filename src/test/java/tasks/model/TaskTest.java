package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.services.TasksService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {

    private Task task;
    private Date start, end;
    private SimpleDateFormat sdf;
    private ArrayTaskList repo;
    private TasksService service;

    @BeforeEach
    void setUp() {
        try {
            task=new Task("new task",Task.getDateFormat().parse("2021-02-12 10:10"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf= Task.getDateFormat();
        try {
            start=sdf.parse("2022-03-29 12:00");
            end=sdf.parse("2022-03-30 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
        repo = new ArrayTaskList();
        service = new TasksService(repo);
    }

    @Test
    void testTaskCreation1() throws ParseException { //unit testing
       assert task.getTitle().equals("new task");
        System.out.println(task.getFormattedDateStart());
        System.out.println(task.getDateFormat().format(Task.getDateFormat().parse("2021-02-12 10:10")));
       assert task.getFormattedDateStart().equals(task.getDateFormat().format(Task.getDateFormat().parse("2021-02-12 10:10")));
    }
    @Test
    void testTaskCreation2() throws ParseException { //unit testing
        task = new Task("TASK2", start, end, 10);
        assert task.getTitle().equals("TASK2");
        assert task.getRepeatInterval()== 10;
        assert task.getStartTime().equals(sdf.parse("2022-03-29 12:00"));
        assert task.getEndTime().equals(sdf.parse("2022-03-30 10:00"));
    }

    @Test
    void step3IntegrationAddTest() throws ParseException { //integration testing
        //integrare E (se testează S + R cu E);
        task=new Task("Task1", start,end, 10);
        service.addTask(task);
        assert(service.getObservableList().size()==1);
    }

    @Test
    void step3IntegrationRemoveTest() throws ParseException{ //integration testing
        //integrare E (se testează S + R cu E);
        task=new Task("Task1", start,end, 10);
        repo.add(task);
        assert (repo.getAll().size() == 1);
        assert(service.getObservableList().size()==1);
        assert(repo.remove(task) == true);
        assert (repo.getAll().size() == 0);
        assert(service.getObservableList().size()==0);
    }

    @AfterEach
    void tearDown() {
    }
}