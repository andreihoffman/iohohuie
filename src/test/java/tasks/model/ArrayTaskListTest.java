package tasks.model;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledOnOs;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.condition.OS.LINUX;
import static org.junit.jupiter.api.condition.OS.MAC;

class ArrayTaskListTest {

    private ArrayTaskList arrayTaskList =new ArrayTaskList();
    private Task task;
    private Date start, end;
    private SimpleDateFormat sdf;
    private static final Logger log = Logger.getLogger(Task.class.getName());

    @BeforeEach
    void setUp() {
        sdf= Task.getDateFormat();
        try {
            start=sdf.parse("2022-03-29 12:00");
            end=sdf.parse("2022-03-29 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Tag("Test1_ECP")
    @DisplayName("😱")
    void ECPTest1() { //TC1
        try {
            arrayTaskList.add(new Task("Titlu valid", start, end, 8));
            assert arrayTaskList.size() == 1;
        }
        catch (IllegalArgumentException e){
            log.error(e.getMessage());
        }
    }

    @Test
    void ECPTest2() {//TC2
        try {
            arrayTaskList.add(new Task("", start, end, 0));
        }
        catch (IllegalArgumentException e){
            assert e.getMessage().equals("Title cannot be empty\nInterval should be >= 1\n");
        }
        assert arrayTaskList.size()==0;
    }

    @Test
    @DisabledOnOs({ LINUX, MAC })
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    void ECPTest3() {
        try {
            arrayTaskList.add(new Task("Titlu valid", start, end, 0));
        }
        catch (IllegalArgumentException e){
            assert e.getMessage().equals("Interval should be >= 1\n");
        }
        assert arrayTaskList.size()==0;
    }

    @Test
    @DisplayName("╯°□°）╯")
    void ECPTest4() {
        try {
            arrayTaskList.add(new Task("", start, end, 8));
            assert arrayTaskList.size() == 1;
        }
        catch (IllegalArgumentException e){
            assert e.getMessage().equals("Title cannot be empty\n");
        }
        assert arrayTaskList.size()==0;
    }


    @Test
    @Disabled("For demonstration purposes")
    void BVATest0(){
        try {
            arrayTaskList.add(new Task("Titlu valid",start,end,12));
        }
        catch (IllegalArgumentException e){
            e.getMessage();
        }
        assert arrayTaskList.size()==1;
    }


    @Test
    @DisplayName("╯°□°）╯")
    void BVATest1(){//TC1
        try {
            arrayTaskList.add(new Task("T",start,end,1));
            assert arrayTaskList.size()==1;
        }
        catch (IllegalArgumentException e){
            log.error(e.getMessage());
        }

    }
    @Test
    void BVATest2(){
        String title="";
        for(int i=0;i<255;i++){
            title+="T";
        }
        try {
            arrayTaskList.add(new Task(title,start,end,Integer.MAX_VALUE-1));
            assert arrayTaskList.size()==1;
        }
        catch (IllegalArgumentException e){
            log.error(e.getMessage());
        }
    }

    @Test
    @Tag("BVATest3")
    void BVATest3(){//TC2
        String title="";
        for(int i=0;i<256;i++){
            title+="T";
        }
        try {
            arrayTaskList.add(new Task(title,start,end,1));
        }
        catch (IllegalArgumentException e){
            assert e.getMessage().equals("Title is too long\n");
        }
        assert arrayTaskList.size()==0;
    }



    @Test
    void BVATest4(){
        try {
            arrayTaskList.add(new Task("Title",start,end,0));
        }
        catch (IllegalArgumentException e){
            assert e.getMessage().equals("Interval should be >= 1\n");
        }
        assert arrayTaskList.size()==0;
    }
}