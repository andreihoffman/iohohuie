package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class TasksOperationsTest {

    private SimpleDateFormat sdf;
    private TasksOperations tasksOperations;
    private Date start, end;

    @BeforeEach
    void setUp() throws ParseException {
        sdf= Task.getDateFormat();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void TC_01() throws ParseException {//TC1
        List<Task> taskList = new ArrayList<>();
        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
        Task t1=new Task("Task2", sdf.parse("2022-04-16 12:00"), sdf.parse("2022-07-20 10:00"), 13);
        Task t2=new Task("Task3", sdf.parse("2022-04-17 12:00"), sdf.parse("2022-05-28 10:00"), 10);
        taskList.add(t);taskList.add(t1);taskList.add(t2);
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);

        start = sdf.parse("2022-04-15 12:00");
        end = sdf.parse("2022-04-10 12:00");
        List<Task> tasks;
        tasks=(List<Task>)tasksOperations.incoming(start,end);
        assert (tasks.size() == 0);
    }
    @Test
    void TC_02() throws ParseException {
        List<Task> taskList = new ArrayList<>();
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);

        start = sdf.parse("2022-04-15 12:00");
        end = sdf.parse("2022-04-20 12:00");
        List<Task> tasks;
        tasks=(List<Task>)tasksOperations.incoming(start,end);
        assert (tasks.size() == 0);
    }
    @Test
    void TC_03() throws ParseException {
        List<Task> taskList = new ArrayList<>();
        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-05-18 10:00"), 1);
        t.setActive(false);
        taskList.add(t);
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);

        start = sdf.parse("2022-04-15 12:00");
        end = sdf.parse("2022-05-10 12:00");
        List<Task> tasks;
        tasks=(List<Task>)tasksOperations.incoming(start,end);
        assert (tasks.size() == 0);
    }

    @Test
    void TC_04() throws ParseException {//TC_02
        List<Task> taskList = new ArrayList<>();
        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2023-05-18 10:00"), 1);
        t.setActive(true);
        taskList.add(t);
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);

        start = sdf.parse("2022-04-11 12:00");
        end = sdf.parse("2024-05-20 12:00");
        List<Task> tasks;
        tasks=(List<Task>)tasksOperations.incoming(start,end);
        assert (tasks.size() == 1);
    }
    @Test
    void TC_05() throws ParseException {
        List<Task> taskList = new ArrayList<>();
        Task t=new Task("Task1", sdf.parse("2022-04-15 12:00"), sdf.parse("2022-04-16 10:00"), 1);
        t.setActive(true);
        Task t1=new Task("Task2", sdf.parse("2022-02-11 12:00"), sdf.parse("2022-04-17 10:00"), 1);
        t1.setActive(true);
        taskList.add(t);taskList.add(t1);
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);

        start = sdf.parse("2022-02-11 12:00");
        end = sdf.parse("2022-03-20 12:00");
        List<Task> tasks;
        tasks=(List<Task>)tasksOperations.incoming(start,end);
        assert (tasks.size() == 1);
        assert (tasks.get(0).getTitle() == "Task2");
    }
}